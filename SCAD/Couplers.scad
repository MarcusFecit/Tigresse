// Couplers
// (c) Marc BERLIOUX, 15 novembre 2016

if(!DontRenderModules){
coupler();

// Rendering
$fs=0.5;
$fa=2.5;
}

module coupler(dOut=20,cHeight=25,s1=5,s2=5){
 color("Azure") {
  difference(){
   cylinder(r=dOut/2,h=cHeight,center=true);
   translate([0,0,cHeight/2-1]) cylinder(r=s1/2,h=cHeight,center=true);
   translate([0,0,-cHeight/2+1])cylinder(r=s2/2,h=cHeight,center=true);
   translate([-13,0,-5]) cube([30,30,1], center=true);
   translate([-13,0,-1]) cube([30,30,1], center=true);
   translate([-13,0,3]) cube([30,30,1], center=true);
   translate([13,0,-3]) cube([30,30,1], center=true);
   translate([13,0,1]) cube([30,30,1], center=true);
   translate([13,0,5]) cube([30,30,1], center=true);
  }
 } 
}
