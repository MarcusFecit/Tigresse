// Tigresse OpenSCAD 3D preview
// (c) Marc BERLIOUX, 25 novembre 2016

include <Bearings.scad>
include <Nema17.scad>
include <Pulleys.scad>
include <Couplers.scad>
include <Nuts.scad>
include <HeatedBeds.scad>
include <PowerSupply.scad>
include <PowerSupplyLid.scad>
include <LeverEndstop.scad>
include <Tigresse_XEndstopSupport.scad>
include <Tigresse_YEndstopSupport.scad>
include <LogoTigresse.scad>
include <LogoLOG.scad>

/////// User Settings /////////////////////////////////////////////

// Axis Positions
XPosition=150;
YPosition=100;
ZPosition=50; // -58 for calibration; -17 touches bed

// Colors
//FrameColor="Chocolate";
//FrameColor="BurlyWood";
//FrameColor="SandyBrown";
FrameColor="Purple";
XEndsColor="LightSalmon";
YCarriageColor="Yellow";
XCarriageColor="DodgerBlue";
RodsColor="PaleTurquoise";
PlasticColor="GreenYellow";

// Show/Hide Assemblies or Elements ( Comment to hide )
showChassis=true;
showProteins=true;
showRods=true;
showRodHolders=true;
showMotors=true;
showPulleys=true;
showIdlers=true;
showBelts=true;
showBearings=true;
showThreadedRods=true;
showCouplers=true;
showXAssy=true;
showYCarriage=true;
showHeatedBed=true;
showPowerSupply=true;
showXCarriageV1=true;
showXEndstop=true;

// Rendering
$fs=0.5;
$fa=2.5;

/////////////////////////////////////////////////////////////////////

Tigresse();

module Tigresse (){
 if (showChassis) Chassis();
 if (showProteins){
  color(PlasticColor)translate([LogoLOGPlateXShift,1,LogoLOGPlateZShift]) rotate([90,0,0])LogoLOGPlate();
  color(PlasticColor)translate([LogoTigressePlateXShift,1,LogoTigressePlateZShift]) rotate([90,0,0])LogoTigressePlate();
  color(PlasticColor)translate([-YSupportsXShift+SheetThickness/2,BackEndYShift-YMotorAxisYShift,YSupportsZShift+25]) rotate([-90,0,-90])YEndstopSupport();
 }
 if (showXEndstop){translate([YSupportsXShift,BackEndYShift-YMotorAxisYShift-5,YSupportsZShift+25+EndstopBodyThickness/2]) rotate([0,0,-90])StraightLeverEndStop();}

 if (showRods) {ZRods();YRods();}
 if(showMotors){YMotor();ZMotors();}
 if (showPulleys) YPulley();
 if (showIdlers) {translate([0,-FrontEndYShift+YIdlerYShift,YSupportsZShift]) rotate([0,90,0])YIdler();}
 if (showThreadedRods) ZThreadedRods();
 if (showCouplers) ZCouplers();
 if (showYCarriage) {
  translate([0,YAxisYPosition,YRodsZShift]){
   YCarriage();
   if (showBearings) YBearings();
  }
  if (showBelts) rotate([90,0,-90]) YBelt();
 }
 if (showXAssy) {
  translate([0,0,XAssyZPosition]) {
   translate([0,-XRodsYShift,0]) {
    translate([-ZRodsXShift,0,0]) XEndMotor();
    if (showProteins){ // X Endstop support
     color(PlasticColor){translate([-ZRodsXShift+30.5+XEndstopSupportWidth/2,0,0])rotate([90,0,90])XEndstopSupport();}
    }
    if (showXEndstop){
//    translate([-ZRodsXShift+30.5+XEndstopSupportWidth/2,-7-EndstopBodyThickness/2,-5])rotate([90,0,0])RoundLeverEndStop();
     translate([-ZRodsXShift+30.5+XEndstopSupportWidth/2,-7-EndstopBodyThickness/2,7])rotate([90,0,0])StraightLeverEndStop();
    }
    translate([ZRodsXShift,0,0]) XEndIdler();
    if (showRods) XRods();
    if (showMotors) XMotor();
    if (showPulleys) XPulley();
    if (showBelts) XBelt();
   }
   if (showBearings) ZBearings();
  }
 }

 if (showXCarriageV1) {
  translate([XCarriageXPosition,-XRodsYShift,XAssyZPosition]){
   XCarriage10mmV1();
   if (showBearings) XCarriage10mmV1Bearings();
  }
 }
 
 if (showPowerSupply){
  translate([SidesXShift+SheetThickness/2,PowerSupplyYShift,PowerSupplyZShift]) 
  rotate([0,90,0])PowerSupply();
 }
 if (showProteins){
  color(PlasticColor)translate([SidesXShift+SheetThickness/2,0.1,FullTopZShift-PowerSupplyLidHeight])PowerSupplyLid(PowerSupplyLidHeight);
 }
}

DontRenderModules=true;

SheetThickness=3;
RodsDiameter=10;
BeltsThickness=1;
LM10UUToPlate=8.077;
PowerSupplyYShift=68;
PowerSupplyZShift=243; // 208 is min; 243 is centered; 272 is max with Lid
PowerSupplyLidHeight=70; // At least 50mm

SidesXShift=170.175;
FrontEndYShift=229;
BackEndYShift=133;
FullTopZShift=416.9;
RodHolderShift=12;
FrameCornerZShift=25;
FrameCornerYShift=77;
FrameCornerXShift=108.525;
EndCornerXShift=91.525;
FrameSideCornerXShift=215.3;
FrameSideCornerZShift=40;
LogoLOGPlateXShift=-196.5;
LogoLOGPlateZShift=299.6327;
LogoTigressePlateXShift=196.0062;
LogoTigressePlateZShift=293.2530;

ZRodsXShift=235.3;
ZRodsYShift=40; //position plaqué au frame = 34.2
ZMotorSupportZShift=51.645;
ZMotorXShift=ZRodsXShift-20;
ZMotorCornerZShift=25;
ZMotorCornerXShift1=24.52; // according to ZMotorSupportXShift..
ZMotorCornerXShift2=29.52; // according to ZMotorSupportXShift..
ZCoupler2NemaZShift=25;
ZRodsLength=FullTopZShift-ZMotorSupportZShift+SheetThickness*1.5;
ZRodsZShift=ZRodsLength/2+ZMotorSupportZShift-SheetThickness/2;

YMotorAxisYShift=31.5; // according to End
YMotorAxisZShift=5; // according to Y Supports screw
YSupportsXShift=8.3; // according to Ends center
YSupportsZShift=25.3876;
YRodsXShift=85.0025;
YRodsYShift=(FrontEndYShift-BackEndYShift+SheetThickness)/2;
YRodsZShift=50; // position où les lm10uu touchent le frame 46.6
YBearingsYShift=35; // according to Bed center
YRodsLength=FrontEndYShift+BackEndYShift+SheetThickness;
YBeltSupportSidesXShift=5.5;
YBeltSupportTopZShift=20.5; // according to Bed
YIdlerYShift=19.67; // according to Front End

XEndFork2Plate=8.5; // according to X Ends plates
XRodsSpacing=45;
XRodsLength=(ZRodsXShift+25)*2;
XRodsYShift=ZRodsYShift-LM10UUToPlate-SheetThickness-XEndFork2Plate;
XEndForkXShift=19.95; // according to bearings
XEndsBearingsZShift=19.15; // according to X Axis
XMotorAxisXShift=46.9203; // according to Z
XIdlerXShift=27; // according to X Axis

XCarriageV1ExtruderZShift=10.3;
XCarriageV1BearingsXShift=16.05;
XCarriageV1CornerZShift=14;
XCarriageV1CornerXShift=35.015;
XCarriageV1BeltXShift=18;
XCarriageV1HeadYShift=39;

XAssyZPosition=FullTopZShift-37-250+ZPosition;
YAxisYPosition=100-ZRodsYShift+XEndFork2Plate-XCarriageV1HeadYShift-YPosition;
XCarriageXPosition=XPosition-150;

echo("<b>Stubs X : ",XRodsLength,"mm</b>");
echo("<b>Stubs Y : ",YRodsLength,"mm</b>");
echo("<b>Stubs Z : ",ZRodsLength,"mm</b>");

module Chassis(){
 color(FrameColor) {
  // Frame
  rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/frame_10mm.dxf",layer="PRINCIPAL");
  // Left Side
  translate([SidesXShift-SheetThickness/2,0,0]) rotate([90,0,90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/left-side_10mm.dxf",layer="PRINCIPAL");
  // Right Side
  translate([-SidesXShift+SheetThickness/2,0,0]) rotate([90,0,-90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/right-side_10mm.dxf",layer="PRINCIPAL");
  // Front End
  translate([0,-FrontEndYShift,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/end_10mm.dxf",layer="PRINCIPAL");
  // Back End
  translate([0,BackEndYShift,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/end_10mm.dxf",layer="PRINCIPAL");
  // Full Top
  translate([0,0,FullTopZShift])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/full-top_10mm.dxf",layer="PRINCIPAL");
  // Frame Corners
  translate([SidesXShift-SheetThickness/2,-FrameCornerYShift-SheetThickness,FrameCornerZShift])
  rotate([0,180,90]) 
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/frame-corner_10mm.dxf",layer="PRINCIPAL");
  translate([-SidesXShift+SheetThickness/2,-FrameCornerYShift-SheetThickness,FrameCornerZShift])
  rotate([0,0,-90]) 
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/frame-corner_10mm.dxf",layer="PRINCIPAL");
  // Ends Corners
  translate([-EndCornerXShift,-FrontEndYShift,FrameCornerZShift])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/frame-corner_10mm.dxf",layer="PRINCIPAL");
  translate([EndCornerXShift,-FrontEndYShift,FrameCornerZShift]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/frame-corner_10mm.dxf",layer="PRINCIPAL");
  translate([-EndCornerXShift,BackEndYShift-SheetThickness,FrameCornerZShift]) rotate([180,0,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/frame-corner_10mm.dxf",layer="PRINCIPAL");
  translate([EndCornerXShift,BackEndYShift-SheetThickness,FrameCornerZShift]) rotate([180,180,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/frame-corner_10mm.dxf",layer="PRINCIPAL");
  // Frame Side Corners
  translate([FrameSideCornerXShift,0,FrameSideCornerZShift-SheetThickness/2]) rotate([0,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/frame-side-corner_10mm.dxf",layer="PRINCIPAL");
  translate([-FrameSideCornerXShift,0,FrameSideCornerZShift+SheetThickness/2]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/frame-side-corner_10mm.dxf",layer="PRINCIPAL");
  // Z motors supports
  translate([ZMotorXShift,0,ZMotorSupportZShift-SheetThickness/2]) rotate([0,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/z-motor-support_10mm.dxf",layer="PRINCIPAL");
  translate([-ZMotorXShift,0,ZMotorSupportZShift+SheetThickness/2]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/z-motor-support_10mm.dxf",layer="PRINCIPAL");
  // Z motors corners
  translate([ZMotorXShift-ZMotorCornerXShift1-SheetThickness/2,0,ZMotorCornerZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/z-motor-corner_10mm.dxf",layer="PRINCIPAL");
  translate([ZMotorXShift+ZMotorCornerXShift2-SheetThickness/2,0,ZMotorCornerZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/z-motor-corner_10mm.dxf",layer="PRINCIPAL");
  translate([-ZMotorXShift+ZMotorCornerXShift1-SheetThickness/2,0,ZMotorCornerZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/z-motor-corner_10mm.dxf",layer="PRINCIPAL");
  translate([-ZMotorXShift-ZMotorCornerXShift2-SheetThickness/2,0,ZMotorCornerZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/z-motor-corner_10mm.dxf",layer="PRINCIPAL");
  // Y Motor Supports
  translate([-YSupportsXShift-SheetThickness/2,BackEndYShift,YSupportsZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/y-motor-support_10mm.dxf",layer="PRINCIPAL");
  translate([YSupportsXShift-SheetThickness/2,BackEndYShift,YSupportsZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/y-motor-support_10mm.dxf",layer="PRINCIPAL");
  // Y Idler Supports
  translate([-YSupportsXShift-SheetThickness/2,-FrontEndYShift,YSupportsZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/y-idler-support_10mm.dxf",layer="PRINCIPAL");
  translate([YSupportsXShift-SheetThickness/2,-FrontEndYShift,YSupportsZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/y-idler-support_10mm.dxf",layer="PRINCIPAL");
  // Y Idler Strainers
  translate([-YSupportsXShift+SheetThickness/2,-FrontEndYShift+YIdlerYShift,YSupportsZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/y-idler-strainer_10mm.dxf",layer="PRINCIPAL");
  translate([YSupportsXShift-SheetThickness*3/2,-FrontEndYShift+YIdlerYShift,YSupportsZShift]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/y-idler-strainer_10mm.dxf",layer="PRINCIPAL");
  if (showRodHolders){
   // Y Rod Holders
   translate([YRodsXShift,-FrontEndYShift-SheetThickness,YRodsZShift-RodHolderShift]) rotate([90,0,0])
   linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/rod-holder_10mm.dxf",layer="PRINCIPAL");
   translate([-YRodsXShift,-FrontEndYShift-SheetThickness,YRodsZShift-RodHolderShift]) rotate([90,0,0])
   linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/rod-holder_10mm.dxf",layer="PRINCIPAL");
   translate([YRodsXShift,BackEndYShift+SheetThickness,YRodsZShift-RodHolderShift]) rotate([90,0,0])
   linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/rod-holder_10mm.dxf",layer="PRINCIPAL");
   translate([-YRodsXShift,BackEndYShift+SheetThickness,YRodsZShift-RodHolderShift]) rotate([90,0,0])
   linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/rod-holder_10mm.dxf",layer="PRINCIPAL");
   // Z Rod Holders
   translate([ZRodsXShift,-ZRodsYShift+RodHolderShift,FullTopZShift+SheetThickness]) rotate([0,0,180])
   linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/rod-holder_10mm.dxf",layer="PRINCIPAL");
   translate([-ZRodsXShift,-ZRodsYShift+RodHolderShift,FullTopZShift+SheetThickness]) rotate([0,0,180])
   linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/rod-holder_10mm.dxf",layer="PRINCIPAL");
  }
 }
}

module ZRods(){
 color(RodsColor){
  translate([ZRodsXShift,-ZRodsYShift,ZRodsZShift])
  cylinder(r=RodsDiameter/2,h=ZRodsLength,center=true);
  translate([-ZRodsXShift,-ZRodsYShift,ZRodsZShift])
  cylinder(r=RodsDiameter/2,h=ZRodsLength,center=true);
 }
}

module YRods(){
 color(RodsColor){
  translate([YRodsXShift,-YRodsYShift,YRodsZShift])rotate([90,0,0])
  cylinder(r=RodsDiameter/2,h=YRodsLength,center=true);
  translate([-YRodsXShift,-YRodsYShift,YRodsZShift])rotate([90,0,0])
  cylinder(r=RodsDiameter/2,h=YRodsLength,center=true);
 }
}

module XRods(){
 color(RodsColor){
   translate([0,0,XRodsSpacing/2])rotate([0,90,0])
   cylinder(r=RodsDiameter/2,h=XRodsLength,center=true);
   translate([0,0,-XRodsSpacing/2])rotate([0,90,0])
   cylinder(r=RodsDiameter/2,h=XRodsLength,center=true);  
 }
}

module XEndMotor(){
 color(XEndsColor) {
  // X End Motor Bearings
  translate([0,-XEndFork2Plate-SheetThickness/2,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/x-end-motor-bearings_10mm.dxf",layer="PRINCIPAL");
  // X End Motor Back
  translate([0,XEndFork2Plate+SheetThickness/2,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/x-end-motor-back_10mm.dxf",layer="PRINCIPAL");
  // X Ends Forks
  translate([XEndForkXShift,0,0]) rotate([90,0,-90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/x-end-fork_10mm.dxf",layer="PRINCIPAL");
  translate([XEndForkXShift,0,0]) rotate([90,0,90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/x-end-fork_10mm.dxf",layer="PRINCIPAL");
  translate([-XEndForkXShift,0,0]) rotate([-90,0,90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/x-end-fork_10mm.dxf",layer="PRINCIPAL");
  translate([-XEndForkXShift,0,0]) rotate([-90,0,-90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/x-end-fork_10mm.dxf",layer="PRINCIPAL");
  // X Nut Clamp
  translate([ZRodsXShift-ZMotorXShift,-LM10UUToPlate-SheetThickness-XEndFork2Plate,SheetThickness])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/x-end-5mm-bread_10mm.dxf",layer="PRINCIPAL");
  translate([ZRodsXShift-ZMotorXShift,-LM10UUToPlate-SheetThickness-XEndFork2Plate,0]) rotate([0,0,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/x-end-5mm-ham_10mm.dxf",layer="PRINCIPAL");
  translate([ZRodsXShift-ZMotorXShift,-LM10UUToPlate-SheetThickness-XEndFork2Plate,-SheetThickness])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/x-end-5mm-bread_10mm.dxf",layer="PRINCIPAL");
 }
 // Nut
 color("Silver"){translate([ZRodsXShift-ZMotorXShift,-LM10UUToPlate-SheetThickness-XEndFork2Plate,0])Nut(8,3.4,2.7);}
}

module XEndIdler(){
 color(XEndsColor) {
  // X End Idler Bearings
  translate([0,-XEndFork2Plate-SheetThickness/2,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/x-end-idler-bearings_10mm.dxf",layer="PRINCIPAL");
  // X End Idler Back
  translate([0,XEndFork2Plate+SheetThickness/2,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/x-end-idler-back_10mm.dxf",layer="PRINCIPAL");
  // X Ends Forks
  translate([0+XEndForkXShift,0,0]) rotate([90,0,-90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/x-end-fork_10mm.dxf",layer="PRINCIPAL");
  translate([0+XEndForkXShift,0,0]) rotate([90,0,90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/x-end-fork_10mm.dxf",layer="PRINCIPAL");
  translate([0-XEndForkXShift,0,0]) rotate([-90,0,90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/x-end-fork_10mm.dxf",layer="PRINCIPAL");
  translate([0-XEndForkXShift,0,0]) rotate([-90,0,-90])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/x-end-fork_10mm.dxf",layer="PRINCIPAL");
  // X Nut Clamp
  translate([-ZRodsXShift+ZMotorXShift,-LM10UUToPlate-SheetThickness-XEndFork2Plate,0+SheetThickness]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/x-end-5mm-bread_10mm.dxf",layer="PRINCIPAL");
  translate([-ZRodsXShift+ZMotorXShift,-LM10UUToPlate-SheetThickness-XEndFork2Plate,0]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/x-end-5mm-ham_10mm.dxf",layer="PRINCIPAL");
  translate([-ZRodsXShift+ZMotorXShift,-LM10UUToPlate-SheetThickness-XEndFork2Plate,0-SheetThickness]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/x-end-5mm-bread_10mm.dxf",layer="PRINCIPAL");
 }
 color("Silver"){translate([-ZRodsXShift+ZMotorXShift,-LM10UUToPlate-SheetThickness-XEndFork2Plate,0])Nut(8,3.4,2.7);}
 if (showIdlers){translate([XIdlerXShift,0,0])rotate([90,0,0])XIdler();}
}

module XIdler(){
 // 2x MF115-ZZ
 translate([0,0,2]) FlangedBallBearing(5,11,4,13,1);
 translate([0,0,-2])rotate([180,0,0]) FlangedBallBearing(5,11,4,13,1);
}

module XBelt(){
 rotate([90,0,0])
 color("Black")linear_extrude(height=6, center=true, convexity=5) import("XBelt_10mm.dxf",layer="XBelt");
}

module YBelt(){
 color("Black")linear_extrude(height=6, center=true, convexity=5) import("YBelt_10mm.dxf",layer="YBelt");
}
 
module YIdler(){
 // 1x 608ZZ
 BallBearing(8,22,7);
}

module YCarriage(){
 color(YCarriageColor) {
  // Bed
  translate([0,0,LM10UUToPlate])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/bed_10mm.dxf",layer="PRINCIPAL");
  // Y Belt Holder
  translate([YBeltSupportSidesXShift,0,LM10UUToPlate]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/y-belt-holder-side_10mm.dxf",layer="PRINCIPAL");
  translate([-YBeltSupportSidesXShift,0,LM10UUToPlate]) rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/y-belt-holder-side_10mm.dxf",layer="PRINCIPAL");
  translate([0,0,LM10UUToPlate-YBeltSupportTopZShift])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/y-belt-holder-top_10mm.dxf",layer="PRINCIPAL");
  translate([0,0,LM10UUToPlate-YBeltSupportTopZShift-SheetThickness*2])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/y-belt-holder-bottom_10mm.dxf",layer="PRINCIPAL");
 }
 // Heated Bed
 if (showHeatedBed) { translate([0,0,LM10UUToPlate+20]) HeatedBed314x214(); }
}

module XCarriage10mmV1Bearings(){
 translate([0,0,XRodsSpacing/2]) rotate([0,90,0])LMxxUU(10,19,29);
 translate([XCarriageV1BearingsXShift,0,-XRodsSpacing/2]) rotate([0,90,0])LMxxUU(10,19,29);
 translate([-XCarriageV1BearingsXShift,0,-XRodsSpacing/2]) rotate([0,90,0])LMxxUU(10,19,29);
}

module ZBearings(){
 translate([-ZRodsXShift,-ZRodsYShift,XEndsBearingsZShift]) LMxxUU(10,19,29);
 translate([-ZRodsXShift,-ZRodsYShift,-XEndsBearingsZShift]) LMxxUU(10,19,29);
 translate([ZRodsXShift,-ZRodsYShift,XEndsBearingsZShift]) LMxxUU(10,19,29);
 translate([ZRodsXShift,-ZRodsYShift,-XEndsBearingsZShift]) LMxxUU(10,19,29);
}

module YBearings(){
 translate([YRodsXShift,YBearingsYShift,0])rotate([90,0,0])LMxxUU(10,19,29);
 translate([YRodsXShift,-YBearingsYShift,0])rotate([90,0,0])LMxxUU(10,19,29);
 translate([-YRodsXShift,YBearingsYShift,0])rotate([90,0,0])LMxxUU(10,19,29);
 translate([-YRodsXShift,-YBearingsYShift,0])rotate([90,0,0])LMxxUU(10,19,29);
}

module XMotor(){
 translate([-ZRodsXShift-XMotorAxisXShift,XEndFork2Plate+SheetThickness,0]) rotate([-90,45,0]) Nema17();
}

module YMotor(){
 translate([-YSupportsXShift-SheetThickness/2,BackEndYShift-YMotorAxisYShift,YSupportsZShift+YMotorAxisZShift]) rotate([0,-90,0]) Nema17();
}

module ZMotors(){
 translate([-ZMotorXShift,-ZRodsYShift,ZMotorSupportZShift-SheetThickness/2]) rotate([180,0,0]) Nema17();
 translate([ZMotorXShift,-ZRodsYShift,ZMotorSupportZShift-SheetThickness/2]) rotate([180,0,0]) Nema17();
}

module XPulley(){
 translate([-ZRodsXShift-XMotorAxisXShift,0,0]) rotate([90,0,0]) 20TeethGT2Pulley();
}
 
module YPulley(){
 translate([0,BackEndYShift-YMotorAxisYShift,YSupportsZShift+YMotorAxisZShift]) rotate([0,90,0]) 20TeethGT2Pulley();
}
 
module XCarriage10mmV1(){
 color(XCarriageColor) {
  // X Carriage Bearings 10mm V1
  translate([0,-LM10UUToPlate,0]) rotate([90,0,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/X-Carriage_10mm_V1/x-carriage-bearings_10mm.dxf",layer="PRINCIPAL");
  // X Carriage Extruder 10mm V1
  translate([0,-LM10UUToPlate-SheetThickness,XCarriageV1ExtruderZShift-SheetThickness]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/X-Carriage_10mm_V1/x-carriage-extruder_10mm.dxf",layer="PRINCIPAL");
  // X Carriage Head 10mm V1
  translate([0,-LM10UUToPlate-SheetThickness-XCarriageV1HeadYShift,XCarriageV1ExtruderZShift-SheetThickness*2]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/X-Carriage_10mm_V1/x-carriage-head_10mm.dxf",layer="PRINCIPAL");
  // X Carriage Corner Fanduct 10mm V1
  translate([XCarriageV1CornerXShift,-LM10UUToPlate-SheetThickness,-XCarriageV1CornerZShift])  rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/X-Carriage_10mm_V1/x-carriage-corner-fanduct_10mm.dxf",layer="PRINCIPAL");
  // X Carriage Corner Servo 10mm V1
  translate([-XCarriageV1CornerXShift,-LM10UUToPlate-SheetThickness,-XCarriageV1CornerZShift])  rotate([0,90,0])
  linear_extrude(height=SheetThickness, center=true, convexity=5) import("../DXF/300x200/X-Carriage_10mm_V1/x-carriage-corner-servo_10mm.dxf",layer="PRINCIPAL");
  // X Carriage Belt Support 10mm V1
  translate([0,0,XCarriageV1ExtruderZShift-SheetThickness]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/X-Carriage_10mm_V1/x-carriage-belt-support_10mm.dxf",layer="PRINCIPAL");
  // X Carriage Belts 10mm V1
  translate([XCarriageV1BeltXShift,0,XCarriageV1ExtruderZShift-SheetThickness*2-BeltsThickness]) 
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/X-Carriage_10mm_V1/x-carriage-belt_10mm.dxf",layer="PRINCIPAL");
  translate([-XCarriageV1BeltXShift,0,XCarriageV1ExtruderZShift-SheetThickness-BeltsThickness]) rotate([0,180,0])
  linear_extrude(height=SheetThickness, center=false, convexity=5) import("../DXF/300x200/X-Carriage_10mm_V1/x-carriage-belt_10mm.dxf",layer="PRINCIPAL");
 }
}

module ZCouplers(){
 translate([ZMotorXShift,-ZRodsYShift,ZMotorSupportZShift-SheetThickness/2+ZCoupler2NemaZShift])coupler();
 translate([-ZMotorXShift,-ZRodsYShift,ZMotorSupportZShift-SheetThickness/2+ZCoupler2NemaZShift])coupler();
}

module ZThreadedRods(){
 color("Silver") {
  translate([ZMotorXShift,-ZRodsYShift,ZMotorSupportZShift-SheetThickness/2+ZCoupler2NemaZShift])
  cylinder(r=2.5,h=350,center=false,$fn=64);
  translate([-ZMotorXShift,-ZRodsYShift,ZMotorSupportZShift-SheetThickness/2+ZCoupler2NemaZShift])
  cylinder(r=2.5,h=350,center=false,$fn=64);
 }
}

