// Tigresse_YEndstopSupport
// (c) Marc BERLIOUX,  8 juillet 2018

// This to generate the modules only when in standalone
if(!DontRenderModules){
 YEndstopSupport();

// Rendering
$fs=0.5;
$fa=2.5;
}

module YEndstopSupport(){
 YMotorSupportWidth=13.4;
 SupportProfileHeight=YMotorSupportWidth+11.5;
 difference(){
  linear_extrude(height=SupportProfileHeight, center=false, convexity=5) import("Tigresse_YEndstopSupport.dxf",layer="SupportProfile");
  translate([0,0,YMotorSupportWidth])
  linear_extrude(height=SupportProfileHeight, center=false, convexity=5) import("Tigresse_YEndstopSupport.dxf",layer="CutOut");
  translate([5,0,YMotorSupportWidth+1.5]) rotate([90,0,0])
  linear_extrude(height=SupportProfileHeight, center=true, convexity=5) import("Tigresse_YEndstopSupport.dxf",layer="EndstopHoles");
  translate([0,5.6,YMotorSupportWidth+1.5])cube([100,10,3],center=true);
 }
}
