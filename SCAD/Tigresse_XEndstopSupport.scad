// Tigresse_XEndstopSupport
// (c) Marc BERLIOUX,  4 juillet 2018

// This to generate the modules only when in standalone
if(!DontRenderModules){
 XEndstopSupport();

// Rendering
$fs=0.5;
$fa=2.5;
}

XEndstopSupportWidth=15;

module XEndstopSupport(){
 difference(){
  linear_extrude(height=XEndstopSupportWidth, center=true, convexity=5) import("Tigresse_XEndstopSupport.dxf",layer="SupportProfile");
  translate([0,7,0])rotate([0,90,0])
  linear_extrude(height=30, center=true, convexity=5) import("Tigresse_XEndstopSupport.dxf",layer="EndstopHoles");
 }
}
