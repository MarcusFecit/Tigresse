// PowerSupply
// (c) Marc BERLIOUX, 24 novembre 2016

if(!DontRenderModules){
 PowerSupply();

// Rendering
$fs=0.5;
$fa=2.5;
}

module PowerSupply(PSHeight=50){
 color("Azure"){
  difference(){
   union(){
    linear_extrude(height=15, center=false, convexity=5) import("PowerSupply.dxf",layer="PS_Outline1");
    linear_extrude(height=PSHeight, center=false, convexity=5) import("PowerSupply.dxf",layer="PS_Outline2");
   }
   linear_extrude(height=10, center=true, convexity=5) import("PowerSupply.dxf",layer="PS_Holes");
  }
 }
}
