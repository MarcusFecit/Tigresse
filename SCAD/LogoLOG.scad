// LogoLOG
// (c) Marc BERLIOUX, 29 juin 2018

if(!DontRenderModules){
 LogoLOGPlate();

// Rendering
$fs=0.5;
$fa=2.5;
}

module LogoLOGPlate(){
 plateHeight=1;
 edgeHeight=3.5;

 translate([0,0,plateHeight/2])
 linear_extrude(height=plateHeight, center = true, convexity=5) import("LogoLOG.dxf",layer="plaquette", $fn=64);

 translate([0,0,edgeHeight/2+plateHeight])
 linear_extrude(height=edgeHeight, center = true, convexity=5) import("LogoLOG.dxf",layer="inside", $fn=64);
}