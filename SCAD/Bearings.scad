// Bearings
// (c) Marc BERLIOUX, 15 novembre 2016

if(!DontRenderModules){
 LMxxUU();
 translate([30,0,0])BallBearing();
 translate([60,0,0])FlangedBallBearing();

// Rendering
$fs=0.5;
$fa=2.5;
}

module LMxxUU(dIn=8,dOut=15,bHeight=24){
 color("Azure") {
  difference(){
   cylinder(r=dOut/2,h=bHeight,center=true);
   cylinder(r=dIn/2,h=bHeight+1,center=true);
  }
 }
}

module BallBearing(dIn=8,dOut=22,t=7){
 color("Gainsboro"){
  difference(){
   cylinder(r=dOut/2,h=t,center=true);
   cylinder(r=dIn/2,h=t+1,center=true);
   difference(){
    cylinder(r=(dIn+3/4*(dOut-dIn))/2,h=t+1,center=true);
    cylinder(r=(dIn+1/4*(dOut-dIn))/2,h=t+1,center=true);
   }
  }
 }
 color("Red"){
  difference(){
   cylinder(r=(dIn+3/4*(dOut-dIn))/2,h=t*0.95,center=true);
   cylinder(r=(dIn+1/4*(dOut-dIn))/2,h=t+1,center=true);
  }
 }
}

module FlangedBallBearing(dIn=5,dOut=11,t=4,dFlange=13,tFlange=1){
 BallBearing(dIn,dOut,t);
 color("Gainsboro"){
  translate([0,0,(t-tFlange)/2]) difference(){
   cylinder(r=dFlange/2,h=tFlange,center=true);
   cylinder(r=dOut/2,h=tFlange+1,center=true);
  }
 }
}
