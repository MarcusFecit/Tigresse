![GitHub Logo](/PNG/Tigresse-Logo.png)

***

This is the repo for the Tigresse, a 3D printer (Reprap) made from laser cut steel sheet

License CC BY-NC 3.0 (see http://creativecommons.org/licenses/by-nc/3.0/)

***

**Features**

* Axis X x Y x Z : 300mm x 200mm x 250mm
* Heated-Bed : 300mm x 200mm
* Rods diameter : 10mm

**Information**

* Français : https://www.logre.eu/wiki/Tigresse
* English  : https://www.logre.eu/wiki/Tigresse/en
* Espanol  : https://www.logre.eu/wiki/Tigresse/es
* Italiano : https://www.logre.eu/wiki/Tigresse/it
* Deutsch  : https://www.logre.eu/wiki/Tigresse/de
