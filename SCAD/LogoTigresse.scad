// LogoTigresse
// (c) Marc BERLIOUX, 29 juin 2018

if(!DontRenderModules){
 LogoTigressePlate();

// Rendering
$fs=0.5;
$fa=2.5;
}

module LogoTigressePlate(){
 plateHeight=1;
 logoHeight=3.5;

 translate([0,0,plateHeight/2])
 linear_extrude(height=plateHeight, center = true, convexity=5) import("LogoTigresse.dxf",layer="Back");

 translate([0,0,logoHeight/2+plateHeight])
 linear_extrude(height=logoHeight, center = true, convexity=5) import("LogoTigresse.dxf",layer="FootPrint");
}