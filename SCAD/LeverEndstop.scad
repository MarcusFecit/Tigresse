// LeverEndstop
// (c) Marc BERLIOUX,  4 juillet 2018

EndstopBodyThickness=6.35;
EndstopSwitchThickness=4;
EndstopLeverWidth=4.15;
EndstopPinsWidth=3.2;

// Colors
EndstopBodyColor="DarkSlateGray";
EndstopLeverColor="LightCyan";
EndstopPinsColor="Gainsboro";
EndstopSwitchColor="Red";

// This to generate the modules only when in standalone
if(!DontRenderModules){
 translate([-20,0,0])RoundLeverEndStop();
 translate([20,0,0])StraightLeverEndStop();

// Rendering
$fs=0.5;
$fa=2.5;
}

module RoundLeverEndStop(){
 color(EndstopBodyColor){linear_extrude(height=EndstopBodyThickness, center=true, convexity=5) import("LeverEndStop.dxf",layer="EndstopBody");}
 color(EndstopSwitchColor){linear_extrude(height=EndstopSwitchThickness, center=true, convexity=5) import("LeverEndStop.dxf",layer="EndstopSwitch");}
 color(EndstopLeverColor){linear_extrude(height=EndstopLeverWidth, center=true, convexity=5) import("LeverEndStop.dxf",layer="EndstopLever1");}
 color(EndstopPinsColor){
  difference(){
   linear_extrude(height=EndstopPinsWidth, center=true, convexity=5) import("LeverEndStop.dxf",layer="EndstopPins");
   translate([-5,0,0])rotate([90,0,0])cylinder(r=0.75,h=30,center=true);
  }
 }
}

module StraightLeverEndStop(){
 color(EndstopBodyColor){linear_extrude(height=EndstopBodyThickness, center=true, convexity=5) import("LeverEndStop.dxf",layer="EndstopBody");}
 color(EndstopSwitchColor){linear_extrude(height=EndstopSwitchThickness, center=true, convexity=5) import("LeverEndStop.dxf",layer="EndstopSwitch");}
 color(EndstopLeverColor){linear_extrude(height=EndstopLeverWidth, center=true, convexity=5) import("LeverEndStop.dxf",layer="EndstopLever2");}
 color(EndstopPinsColor){
  difference(){
   linear_extrude(height=EndstopPinsWidth, center=true, convexity=5) import("LeverEndStop.dxf",layer="EndstopPins");
   translate([-5,0,0])rotate([90,0,0])cylinder(r=0.75,h=30,center=true);
  }
 }
}

