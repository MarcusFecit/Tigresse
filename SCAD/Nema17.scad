// Nema17
// (c) Marc BERLIOUX, 15 novembre 2016

if(!DontRenderModules){
 Nema17();

// Rendering
$fs=0.5;
$fa=2.5;
}

module Nema17(nHeight=42,sLength=20){
 difference(){
  union(){
   color("DarkGray") linear_extrude(height=10, center=false, convexity=5) import("Nema17.dxf",layer="17Body", $fn=64);
   translate([0,0,10])
   color("Black") linear_extrude(height=nHeight-20, center=false, convexity=5) import("Nema17.dxf",layer="17Body2", $fn=64);
   translate([0,0,nHeight-10])
   color("DarkGray") linear_extrude(height=10, center=false, convexity=5) import("Nema17.dxf",layer="17Body", $fn=64);
   translate([0,0,-2])
   color("DarkGray") linear_extrude(height=2, center=false, convexity=5) import("Nema17.dxf",layer="17ColletHole", $fn=64);
   translate([0,0,-sLength-2])
   color("LightSkyblue") linear_extrude(height=sLength, center=false, convexity=5) import("Nema17.dxf",layer="5mmShaft", $fn=64);
  }
  translate([0,0,-5])
  linear_extrude(height=nHeight+10, center=false, convexity=5) import("Nema17.dxf",layer="17Holes", $fn=64);
 }
}
