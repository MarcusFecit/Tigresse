// PowerSupplyLid
// (c) Marc BERLIOUX, 28 juin 2018

// Settings
LidHeight=70; // Minimum is 50mm
BottomClosedLid=false; // Default is 'false' ; Put it to 'true' if you want a bottom closed Lid
LogoType=2; // Tigresse FootPrint=1; Electric Shock Warning=2;

// Nuts and screws housings
ScrewDiameter=3.5;
ScrewLength=12;
NutWrench=5.7;
NutThickness=2.5;

// This to generate the Lid only when in standalone
if(!DontRenderModules){
 PowerSupplyLid(LidHeight);

// Rendering
$fs=0.5;
$fa=2.5;
}

// Debugging ..
//LidFixation();
//NutCutOut();
 

module PowerSupplyLid(LH=70){
 difference(){
  union(){
   linear_extrude(height=LH, center=false, convexity=5) import("PowerSupplyLid.dxf",layer="Lid");
   translate([50,-3,LH-24.9])cube([4,3,9.8]);
   translate([50,-3,LH-49.9])cube([4,3,9.8]);
   translate([49,0,LH-32.5])rotate([-90,0,0])LidFixation();
   translate([49,67.5439,LH])rotate([180,0,0])LidFixation();
   translate([0,124,LH-30])rotate([0,90,0])LidFixation();
  }
  translate([49,-0.1,LH-32.5])rotate([-90,0,0])cylinder(r=ScrewDiameter/2,h=ScrewLength+2);
  translate([49,67.5439,LH+0.1])rotate([180,0,0])cylinder(r=ScrewDiameter/2,h=ScrewLength+2);
  translate([-0.1,124,LH-30])rotate([0,90,0])cylinder(r=ScrewDiameter/2,h=ScrewLength+2);
  translate([49,-NutThickness/2+ScrewLength-5,LH-32.5])rotate([90,0,0])NutCutOut();
  translate([49,67.5439,LH+NutThickness/2-ScrewLength+5])rotate([0,0,0])NutCutOut();
  translate([-NutThickness/2+ScrewLength-5,124,LH-30])rotate([90,0,90])NutCutOut();
  LogoPosition=(LH-50)/2+35;
  if(LogoPosition>=35){
   if(LogoType==1){
    translate([53.51,67,LH-LogoPosition])rotate([90,0,90])
    linear_extrude(height=1, center = true, convexity=5) import("LogoTigresse.dxf",layer="FootPrint");
   }
   if(LogoType==2){
    translate([53.51,67,LH-LogoPosition])rotate([90,0,90])
    linear_extrude(height=1, center = true, convexity=5) import("ElectricShockWarning.dxf",layer="PRINCIPAL");
   }
  }
 }
 if(BottomClosedLid){cube([50,129,4]);}
}

module LidFixation(){
   cylinder(r=5,h=ScrewLength-2);
   translate([0,0,ScrewLength-2])sphere(r=5);
}

module NutCutOut(){
 cube([20,NutWrench,NutThickness],center=true);
}
